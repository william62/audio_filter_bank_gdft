% Near perfect reconstruction polyphase filter bank demo
%
% (c) 2007-2010 Wessel Lubberhuizen

%clear all;
%close all;

% number of taps per channel 
L = 32; 
% number of channels
N = 128;
K=10;
%npr_coeff(N,L,K)

display('designing prototype filter...');

%c=npr_coeff_poly(N,L, [0.999999 0.999999 0.980159 1/sqrt(2)]);
c=npr_coeff(N,L, K);
%c = npr_coeff_poly(N,L,[0.999999 0.999999 0.980159 1/sqrt(2)]);
c = real(c);
display('generating a test signal...');
M = 4096; % number of slices

sampling = 16000;
duration = 1;
freq = [10: 20: sampling];
time_step = linspace(0, duration, duration * sampling);
figure(111);hold all;
channel = zeros(N, length(freq));
for ii = 1:length(freq)
    x = sin(time_step * freq(ii)*pi*2);
    y=npr_analysis(c,x);
    y_psd = mean(abs(y), 2);
    %figure(112);plot(y_psd);
    channel(:, ii) = 20*log10(y_psd);
end

for ii = 1:2
    figure(111);hold all;
    plot(freq, channel(ii,:));hold all;
end
figure(111);hold all;
set(gca, "linewidth", 2, "fontsize", 20)
xlim([0, 600])
ylim([-60, 0])
grid on;
title('oversampled (rate = 2) filter banks ')
xlabel('freq')
ylabel('db')



