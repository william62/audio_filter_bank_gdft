% Near perfect reconstruction polyphase filter bank demo
%
% (c) 2007-2010 Wessel Lubberhuizen

clear all;
close all;

% number of taps per channel 
L = 8; 
% number of channels
N = 128;
K=11.4;
%npr_coeff(N,L,K)

display('designing prototype filter...');

%c=npr_coeff(N,L,K);
c=npr_coeff(N,L);

display('generating a test signal...');
M = 4096; % number of slices

c = real(c);


Frame2test = 10;
sampeNo = 512*Frame2test;
startsample = 30000;
[X,FS]=audioread('HelloJio_x50_Pi_0226_MSM_capture.wav');
x = X(startsample: startsample + sampeNo - 1)';
length(x)
% add some white noise if you like
%x = awgn(x,200);

% run it through the npr filterbank
display('processing...');
time1 = cputime;
y=npr_analysis(c,x);
z=npr_synthesis(c,y);
time2=cputime;
display(sprintf('processing rate = %f kSamples / second',length(x)/(time2-time1)/1E3));

% compare the input and the output
delay=N*(L-1)/2;
padding = zeros(1,delay);
z = z(1+delay:length(z));
x = x(1:length(x)-delay);

display(sprintf('average reconstruction error = %f dB',20*log10(norm(z-x,2)/norm(x,2))));

figure(111);
subplot(2,1,1);
plot(real([x' z']));
title('input / output signals');
xlabel('sample');
ylabel('signal value');
grid on;
subplot(2,1,2);
plot(real(x'-z'));
xlabel('time (sample)');
ylabel('error value');
grid on;

figure(222);
plot(20*log10(abs([x' z'-x'])));
title('reconstruction error');
xlabel('time (sample)');
ylabel('power (dB)');
grid on;

figure(333);
pcolor(abs(y(1:N/2,:))')

