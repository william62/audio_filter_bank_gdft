import scipy
from scipy.io.wavfile import read
import matplotlib.pyplot as plt
import numpy as np
from scipy.signal import firwin, freqz, lfilter, welch 
np.random.seed(1)
fs = 44100

def db(x):
    """ Convert linear value to dB value """
    return 10*np.log10(x)

def gen_complex_chirp(fs, pad_frac=.01, time_s=1):
    f0= -fs / (2. * (1 + pad_frac))
    f1= fs / (2. *(1 + pad_frac))
    t1 = time_s
    beta = (f1 - f0) / float(t1)
    t = np.arange(0, t1, t1/ float(fs))
    #print(len(t))
    #plot.plot(np.exp(2j * np.pi * (.5 * beta * (t ** 2) + f0 * t)).T)
    return np.exp(2j * np.pi * (.5 * beta * (t ** 2) + f0 * t))
    #return np.random.normal(0, 1, 10000).cumsum()

def gen_noisy_chirp():
    #Add noise to make it a little less synthetic looking
    chirp = gen_complex_chirp(fs) 
    return chirp + 1.5 * (np.random.randn(*chirp.shape) + 1j * np.random.randn(*chirp.shape))
    #return chirp

#def plot_specgram(d, xlim=20000):
#    plt.specgram(d, NFFT=512, Fs=fs, cmap='gist_earth')
#    ax = plt.gca()
#    #Remove whitespace on plot
#    ax.set_xlim(0, xlim)
    #ax.set_ylim(-1, 1)
#    ax.set_ylabel("Normalized complex frequency")
#    ax.set_xlabel("Time (samples)")
    

#input_data = read("zf.wav")
#audio = input_data[1]
#audio = [1, 2, 3, 4, 5]
#plot.plot(audio)
#plot.ylabel("Amplitude")
#plot.xlabel("Time (samples)")
#plot.title("test sample")
#plot.show()

def plot_filter(f, cx=False):
    w, h = freqz(f)
    plt.plot(w / max(w), np.abs(h), color="steelblue")
    if cx:
        plt.plot(-w / max(w), np.abs(h), color="darkred")
    ax = plt.gca()
    ax.set_xlabel("Normalized frequency")
    ax.set_ylabel("Gain")
    return

def plot_specgram(input, filtered_input, xlim = 1):
    fig, (ax1, ax2) = plt.subplots(nrows=2, figsize=(8, 10))
    values, ybins, xbins, im = ax1.specgram(input, NFFT = 512, Fs = fs, scale_by_freq=True, cmap='gist_earth')
    ax1.set(title='Specgram of raw input')
    ax1.set_ylim(-20000, 20000)
    ax1.set_xlim(0, xlim)
    fig.colorbar(im, ax=ax1)
    values, ybins, xbins, im  = ax2.specgram(filtered_input, NFFT = 512, Fs = fs, scale_by_freq=True, cmap='gist_earth')
    ax2.axis('tight')
    ax2.set_ylim(-20000, 20000)
    ax2.set_xlim( 0, xlim)
    ax2.set(title='Specgram of filtered input')
    fig.colorbar(im, ax=ax2)


def polyphase_core(x, m, f):
    #x = input data
    #m = decimation rate
    #f = filter
    #append zeros to match decimation rate
    if x.shape[0] % m != 0:
        x = np.append(x, np.zeros((m - x.shape[0] % m,)))
    if f.shape[0] % m != 0:
        f = np.append(f, np.zeros((m - f.shape[0] % m,)))
    pp = np.zeros((m, int((x.shape[0] + f.shape[0]) / m)), dtype = x.dtype)
#    print(x.dtype)
    pp[0, :-1 ] = np.convolve(x[::m], f[0::m])
    #print(m)
    #print(f[::m])
#    print(pp)
    #Invert the x values when applying filters
    for i in range(1, m):
#        print(m)
        pp[i, 1:] = np.convolve(x[m - i::m], f[i::m])
        #print(f[i::m])
    return pp


def polyphase_expansion(xx, f):
    #xx = input data array
    #f = filter
    #append zeros to match decimation rate
    # xx.shape[0] will be m, the expansion rate
    m = xx.shape[0]
    ## filter length needs to be m
    if f.shape[0] % m != 0:
        f = np.append(f, np.zeros((m - f.shape[0] % m,)))

    ppx = np.zeros((m, int(xx.shape[1] + f.shape[0]/m )), dtype = xx.dtype)
    #Invert the x values when applying filters
    for i in range(0, m):
        #print(i)
        ff = f[m - 1 - i::m]
        #print(ff)
        ff = np.flip(ff)
        #print(ff)
        ppx[i, 0: -1] = np.convolve(xx[i, :],  ff)
    #print(ppx[:,0].real)
    #print(ppx[:,1].real)
    print(np.reshape(ppx, -1, order = 'F')[0:m+1].real)
    return np.reshape(ppx, -1, order = 'F')

def polyphase_expansion_single(xx, f, m):
    #xx = input data array
    #f = filter
    #append zeros to match decimation rate

    ppx = np.zeros((m, xx.shape[0] + int(f.shape[0]/m) ) , dtype = xx.dtype)
    #Invert the x values when applying filters
    for i in range(0, m):
#        print(m)
        ppx[i, 0:-1] = np.convolve(xx, f[i::m])
    return np.reshape(ppx, -1, order = 'F')

def polyphase_single_filter(x, m, f):
    return np.sum(polyphase_core(x, m, f), axis = 0)

def generate_test_realsig(duration = 1, step = 10000, Freq = 100):
    time_tick = np.linspace(0, duration, step)
    return np.sin(time_tick*np.pi*2*Freq) + np.random.randn(step)

def polyphase_synthesis_bb(ss, m, filt):
    up_filter_in = np.fft.ifft(ss, n=m, axis=0)*m*m
    #up_filter_in = ss
    print(up_filter_in.shape)
    #print(up_filter_in[:,0])
    #print(np.reshape(up_filter_in, -1, order = 'F')[0:129])    
    return polyphase_expansion(up_filter_in, filt)

def polyphase_analysis(x, filt):

    nn = filt.shape[0]

    if x.shape[0] % nn != 0:
        x = np.append(x, np.zeros((nn - x.shape[0] % nn,)))    
    
    mm = int(x.shape[0]/nn)
    xx1 = x.reshape((nn,mm), order = 'F')
    xx2 = xx1.astype('complex')

    for ii in range(nn):
        xx2[ii, :] = xx2[ii, :]*np.exp(1j*np.pi*(ii-1)/nn)
        xx2[ii, 1::2] = -xx2[ii, 1::2]

    filt = np.fliplr(filt)
    out1 = np.zeros((nn, mm + filt.shape[1] - 1))
    out2 = np.zeros((nn, mm + filt.shape[1] - 1), dtype = 'complex')
    

    out = np.zeros((nn*2, mm + filt.shape[1] - 1), dtype = 'complex')   
    for ii in range(nn):
        out1[ii,:] = np.convolve(xx1[ii,:], filt[ii,:])    
        out2[ii,:] = np.convolve(xx2[ii,:], filt[ii,:])   

    out1 = np.fft.ifft(out1, n=nn, axis=0)*nn
    out2 = np.fft.ifft(out2, n=nn, axis=0)*nn

    for ii in range(nn):
        out[2*ii, :] = out1[ii, :]
        out[2*ii + 1, :] = out2[ii, :]

    return out

def polyphase_synthesis(x, filt):

    nn = filt.shape[0]
    
    mm = x.shape[1]
    yy1 = np.zeros((nn, mm), dtype = 'complex')
    yy2 = np.zeros((nn, mm), dtype = 'complex')
    
    for ii in range(nn):
        yy1[ii, :] = x[2*ii, :]
        yy2[ii, :] = x[2*ii + 1, :]

    yy1 = np.fft.fft(yy1, n=nn, axis=0)*nn
    yy2 = np.fft.fft(yy2, n=nn, axis=0)*nn    
    
    out1 = np.zeros((nn, mm + filt.shape[1] - 1), dtype = 'complex')
    out2 = np.zeros((nn, mm + filt.shape[1] - 1), dtype = 'complex')    
    
    for ii in range(nn):
        out1[ii,:] = np.convolve(yy1[ii,:], filt[ii,:])    
        out2[ii,:] = np.convolve(yy2[ii,:], filt[ii,:]) 

    for ii in range(nn):
        out2[ii, :] = out2[ii, :]*np.exp(-1j*np.pi*(ii-1)/nn)
        out2[ii, 1::2] = -out2[ii, 1::2]

    return np.reshape(out1 - out2, -1, order = 'F')

def pfb_spectrometer(filt, duration = 1, sampling = 16000, figNo = 666):
    
    m = filt.shape[0]*2
    freq = np.arange(10, sampling, 10)
    time_step = np.linspace(0, duration, duration*sampling)
    #print(freq.shape[0])
    chan = np.zeros((m, freq.shape[0]))
    syn_pow_out = np.zeros((1, freq.shape[0]))
    #synthesis_out = np.zeros((1, freq.shape[0]))


    for fidx in range(0,freq.shape[0]):
        
        x = np.sin(time_step * freq[fidx]*np.pi*2)
        #plt.plot(time_step, x)
        fft = polyphase_analysis(x, filt)
        x_psd = np.abs(fft)**2
        x_psd = x_psd.mean(axis=1)
        chan[:,fidx] = db(x_psd)
        #synthesis_out = polyphase_synthesis(fft, m, filt)
        #syn_pow_out[fidx] = db(np.abs(np.fft.fft(synthesis_out, axis = 1))**2)

    
    plt.figure(figNo)
    for idx in range(0, m):
        plt.plot(freq, chan[idx,:])
    plt.ylabel("Power [dB]")
    plt.xlabel("freq")

    return chan


def pfb_ana_syn_val(filt, duration = 1, sampling = 16000, figNo = 888, freqRange = [0], frame = 512 ):
    
    nn = filt.shape[0]
    L = filt.shape[1]
    freq = np.arange(10, sampling, 100)
    time_step = np.linspace(0, duration, duration*sampling)
    #if time_step.shape[0] % frame != 0:
    #    time_step = np.append(time_step, np.zeros((frame - time_step.shape[0] % frame,)))
    #print(freq.shape[0])
    chan = np.zeros((nn*2, freq.shape[0]))
    #syn_pow_out = np.zeros((len(freqRange), freq.shape[0]))
    #synthesis_out = np.zeros((len(freqRange), time_step.shape[0]))

    for fidx in freqRange:
        #test_in = np.sin(time_step * freq[fidx]*np.pi*2)
        test_in = np.random.randn(duration*sampling)
        fft = polyphase_analysis(test_in, filt)
        synthesis_out = polyphase_synthesis(fft, filt)
        # get rid of the filter delay
        test_out = synthesis_out[int(2*nn*(L-1)/2):].real
        test_out = test_out[:test_in.shape[0]]
        #test_in = test_in[0:] # get rid of window effects
        #test_out = test_out[0:test_in.shape[0]]
        plt.figure(figNo)
        plt.plot( test_in, 'r')
        plt.plot( test_out, 'b')
        #plt.figure(figNo + 3)
        #plt.plot( synthesis_out.real, 'b')
        plt.figure(figNo + 2)
        plt.plot( test_in - test_out, 'b')     

        freq_in, psd_in = welch(test_in, fs=sampling )   
        freq_err, psd_err = welch(test_out - test_in, fs=sampling )   
        plt.figure(figNo + 1)
        plt.plot(freq_in, db(psd_in) - db(psd_err))        
    return 

m = 128

plt.figure(201)

filt_poly = np.loadtxt('testfilter.txt', dtype='float', comments='#')
pfb_spectrometer(filt_poly)
#plt.show()


#poly_filter_Syn = np.loadtxt('testFilterAnalysis.txt', dtype='float', comments='#')
#subband_out = pfb_spectrometer(m, filt )
#pfb_ana_syn_val(filt_poly)



# example of loading wav file and decode into subband and then synthesis

FS, input_data = read("HelloJio_x50_Pi_0226_MSM_capture.wav")
Frame2test = 10
sampeNo = 512*Frame2test
startsample = 30000
wave_scale = 2**-15
analysis_in = np.asarray(input_data[startsample: startsample + sampeNo - 1], dtype = 'float')*wave_scale

nn = filt_poly.shape[0]
L = filt_poly.shape[1]

synthesis_in = polyphase_analysis(analysis_in, filt_poly)
synthesis_out = polyphase_synthesis(synthesis_in, filt_poly)
       # get rid of the filter delay
test_out = synthesis_out[int(2*nn*(L-1)/2):].real
test_out = test_out[:analysis_in.shape[0]]

figNo = 999

plt.figure(figNo)
plt.plot( analysis_in, 'r')
plt.plot( test_out, 'b')
ax = plt.gca()
ax.set_xlabel("sampling tick")
ax.set_ylabel("normalized amplitude [-1, 1]")
ax.set_title("wav signal vs. reconstruction time domain")
#plt.figure(figNo + 3)
#plt.plot( test_out, 'b')
plt.figure(figNo + 2)
plt.plot( analysis_in - test_out, 'b')     
ax = plt.gca()
ax.set_xlabel("sampling tick")
ax.set_ylabel("normalized amplitude [-1, 1]")
ax.set_title("reconstruction error in time domain")

freq_in, psd_in = welch(analysis_in, fs=FS )   
freq_err, psd_err = welch(test_out - analysis_in, fs=FS )   
plt.figure(figNo + 1)
plt.plot(freq_in, db(psd_in) - db(psd_err)) 
ax = plt.gca()
ax.set_xlabel("frequency Hz")
ax.set_ylabel("db")
ax.set_title("reconstruction error in db vs. freq")
plt.show()
exit
