%NPR_COEFF generates near NPR filter bank coefficients
%  COEFF = NPR_COEFF(N,L) generates the filter coefficients
%  for a near perfect reconstruction filter bank.
%  The number of subbands will be N, and L is the number of 
%  filter taps used per subband. The output COEFF will have 
%  size (N/2,L).
%
%  The prototype is constructed starting with an equiripple
%  approximation to a 'root raised error function' shaped filter.
%
%  NPR_COEFF(N,L) with no output arguments plots the magnitude
%  and the prototype filter in the current figure window.
%
%  See also npr_analysis, npr_synthesis, npr_coeff
%
% (c) 2007 Wessel Lubberhuizen
function coeff = npr_coeff_remez(N, L)

M = N /2;

F= (0:(L*M-1))/(L*M);

B = load('testFilterTaps.m');

%B=fftshift(B);


    N=16384;
    F=(0:N-1)/N;

    F=F-0.5;
    W=2*pi*F;
    H=freqz(B,1,W);
    
    figure(333);hold all;
    plot(2*F*M,20*log10(abs(H')));hold all;
    axis([-M M -400 0]);  grid on;      
    
    xlabel('frequency (channel)');
    ylabel('reconstr. error (dB)');

    grid on;
    coeff=[];
    figure(333);

    B=B/sum(B);
    coeff = reshape(B,M,L);



function A = rrerf(F,K,M)

x = K*(2*M*F-0.5);
A= sqrt(0.5*erfc(x));