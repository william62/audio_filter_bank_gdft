%NPR_COEFF generates near NPR filter bank coefficients
%  COEFF = NPR_COEFF(N,L) generates the filter coefficients
%  for a near perfect reconstruction filter bank.
%  The number of subbands will be N, and L is the number of 
%  filter taps used per subband. The output COEFF will have 
%  size (N/2,L).
%
%  The prototype is constructed starting with an equiripple
%  approximation to a 'root raised error function' shaped filter.
%
%  NPR_COEFF(N,L) with no output arguments plots the magnitude
%  and the prototype filter in the current figure window.
%
%  See also npr_analysis, npr_synthesis, npr_coeff
%
% (c) 2007 Wessel Lubberhuizen
function [coeff, coeff_poly] = npr_coeff_poly(N,L,K)

if ~exist('N','var')
    N=256;  % if the number of subband is not specified, use a default value
end

if ~exist('L','var');
    L=128; % if the number of taps is not specified, use a default value
end

if ~exist('K','var');
    % if the K value is not specified us a default value.
    % these values minimize the reconstruction error.
    switch(L)
        case 8,    K=4.853;
        case 10,   K=4.775;
        case 12,   K=5.257;
        case 14,   K=5.736;
        case 16,   K=5.856;
        case 18,   K=7.037;
        case 20,   K=6.499;
        case 22,   K=6.483;
        case 24,   K=7.410;
        case 26,   K=7.022;
        case 28,   K=7.097;
        case 30,   K=7.755;
        case 32,   K=7.452;
        case 48,   K=8.522;
        case 64,   K=9.396;
        case 96,   K=10.785;
        case 128,  K=11.5; 
        case 192,  K=11.5;
        case 256,  K=11.5;
        otherwise, K=8;
    end
end


M = N/2;
F= (0:(L*M-1))/(L*M);

% The prototype is based on a root raised error function

P = zeros(1, L - 1 );
for ii = 1:L/2 - 1
    P(ii) = K(ii);
    P(L - ii) = sqrt(1 - P(ii)^2);
end

P = [1 P];

pp = zeros(1, L*M);
for ii = 1:L*M
    temp = 0;
    for jj = 1:L - 1
      temp = temp + (-1)^jj * P(jj + 1) * cos(2*pi*jj/(L*M)*(ii + 1/2));
    end
    pp(ii) = P(1) + 2*temp;
end

B = pp;
%B=fftshift(B);
B=B/sum(B);

if nargout==0
    % if not output arguments are specified, 


    N=16384;
    F=(0:N-1)/N;

    F=F-0.5;
    W=2*pi*F;
    H=freqz(B,1,W);
    figure(222);hold all;
    plot(2*F*M,20*log10(abs(H')));hold all;
    axis([-2 2 -100 0]);
    xlabel('frequency (channel)');
    ylabel('power (dB)');
    grid on;
    

else
    %B=B/sum(B);
    coeff = reshape(B,M,L);
end


function A = rrerf(F,K,M)

x = K*(2*M*F-0.5);
A= sqrt(0.5*erfc(x));